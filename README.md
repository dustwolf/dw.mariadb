# Ansible MariaDB

Ansible role for installing the MariaDB database server.

Currently made to work with CentOS 8 stream Linux.

## Note

As MariaDB ships with a `mysql_secure_installation` script that is meant to be run interactivelly, this role preforms all the steps of the script in a noninteractive manner.

As `mysql_secure_installation` is native bash mysql, this code is done in such a way that there are no special dependencies.

This code cannot change the root password, it is only configured the first time.

See [defaults file](defaults/main.yml) for configurables.

## Project status
This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.
